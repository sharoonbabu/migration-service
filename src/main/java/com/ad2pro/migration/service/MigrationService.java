package com.ad2pro.migration.service;


import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import sun.misc.ClassLoaderUtil;

import java.io.*;
import java.lang.reflect.Array;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
@Slf4j
public class MigrationService {



    @Autowired
    private MigrationService migrationService;

    private int batchUploadRowNo = 1;

    private int maxCols = 0;

    Map<Integer, String> categories = new HashMap<>();

    public void migration(String filepath) {

        Workbook workbook;

        String fileName = "/home/devteam/Downloads/migration/batchupload.xlsx";
        XSSFWorkbook batchUploadWorkbook = new XSSFWorkbook();
        XSSFSheet batchUploadSheet = batchUploadWorkbook.createSheet("Batch Upload");
        Row firstRow = batchUploadSheet.createRow(0);

        List<String> newHeaders = Arrays.asList("ASSET ID", "CATEGORY","ASSET TYPE","ASSET CODE","TITLE","MEDIA TYPE","TAGS");

        for(int i=0; i<newHeaders.size(); i++){
            firstRow.createCell(i).setCellValue(newHeaders.get(i));
        }


        try {
            try (FileInputStream excelFile = new FileInputStream(new File(filepath))) {
                workbook = new XSSFWorkbook(excelFile);

            }

            getCategories();

            XSSFSheet dataTypeSheet = (XSSFSheet) workbook.getSheetAt(0);
            Row row = dataTypeSheet.getRow(0);
            List<String> headers = getHeaders(row);

            Map<String, Object> assetMetadata = new HashMap<>();
            Map<Integer,Map<String, Object>> versions = new HashMap<>();


            for (Row currentRow : dataTypeSheet) {

                if (currentRow.getRowNum() == 0) continue;
                for (Cell currentCell : currentRow) {
                    int columnIndex = currentCell.getColumnIndex();
                    if (headers.get(columnIndex).equalsIgnoreCase("asset_id")) {
                        currentCell.setCellType(CellType.STRING);
                            String value = currentCell.getStringCellValue();
                            assetMetadata.put("Asset Id",value);


                    } else if (headers.get(columnIndex).equalsIgnoreCase("asset_code")) {

                        currentCell.setCellType(CellType.STRING);
                        String value = currentCell.getStringCellValue();
                        assetMetadata.put("Asset Code",value);



                    } else if (headers.get(columnIndex).equalsIgnoreCase("site_id")) {



                    } else if (headers.get(columnIndex).equalsIgnoreCase("asset_title")) {
                        String value = currentCell.getStringCellValue();
                        if (!StringUtils.isEmpty(value)) {
                            assetMetadata.put("Title",value);
                        }

                    } else if (headers.get(columnIndex).equalsIgnoreCase("asset_type")) {
                        String value = currentCell.getStringCellValue();
                        if (!StringUtils.isEmpty(value)) {
                           assetMetadata.put("Asset Type", value);
                        }


                    } else if (headers.get(columnIndex).equalsIgnoreCase("media_type")) {

                        String value = currentCell.getStringCellValue();
                        if (!StringUtils.isEmpty(value)) {
                            assetMetadata.put("Media Type", value);
                        }

                    } else if (headers.get(columnIndex).equalsIgnoreCase("am_asset_category")) {
                        currentCell.setCellType(Cell.CELL_TYPE_STRING);
                        String value = currentCell.getStringCellValue();
                        List<String> categValues = new ArrayList<>();
                        if (!StringUtils.isEmpty(value)) {
                            List<String> list  = Arrays.asList(value.split(","));
                            for(String categId : list){
                                categValues.add(categories.get(Integer.parseInt(categId)));
                            }
                            String finalCategories = String.join(",",categValues);
                            assetMetadata.put("Category",finalCategories);
                        }
                    } else if (headers.get(columnIndex).equalsIgnoreCase("asset_files")) {

                        String value = currentCell.getStringCellValue();
                        if (!StringUtils.isEmpty(value)) {
                            double versionNo = currentRow.getCell(headers.indexOf("version")).getNumericCellValue();
                            Map<String, Object> version = null;
                               version = versions.get((int) versionNo);

                            List<String> additionalFiles = null;

                            if(null == version)
                                version = new HashMap<>();
                            if(currentRow.getCell(headers.indexOf("af_file_type")).getStringCellValue().equals("M"))
                                version.put("primary",value);
                            else {
                                 additionalFiles = (List<String>) version.get("additional");
                                if(null == additionalFiles)
                                    additionalFiles = new ArrayList<>();
                                additionalFiles.add(value);
                                version.put("additional",additionalFiles);
                            }

                                versions.put((int) versionNo, version );
                        }
                    } else if (headers.get(columnIndex).equalsIgnoreCase("tags")) {

                        String value = currentCell.getStringCellValue();
                        if (!StringUtils.isEmpty(value)) {
                            assetMetadata.put("Tags", value);
                        }
                    }
                }
                Row nextRow = dataTypeSheet.getRow(currentRow.getRowNum() + 1);
                double value = -1;
                if (nextRow != null)
                    value = nextRow.getCell(headers.indexOf("asset_id"), Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getNumericCellValue();

                if(!String.valueOf((int)value).equals(currentRow.getCell(headers.indexOf("asset_id")).getStringCellValue()) || nextRow ==null){

                    if(maxCols < versions.size())
                        maxCols = versions.size();

                    Row newRow = batchUploadSheet.createRow(batchUploadRowNo++);
                    int colNum=0;
                    for (Map.Entry<String, Object> entry : assetMetadata.entrySet()) {
                        Object field = entry.getValue();
                        Cell cell = newRow.createCell(colNum++);
                        if (field instanceof String) {
                            cell.setCellValue((String) field);
                        } else if (field instanceof Integer) {
                            cell.setCellValue((Integer) field);
                        }
                    }
                    for(Map.Entry<Integer, Map<String, Object>> entry : versions.entrySet()){
                        String primaryFile = (String) entry.getValue().get("primary");
                        Cell cell = newRow.createCell(colNum++);
                        cell.setCellValue(primaryFile);

                        Cell acell = newRow.createCell(colNum++);
                        if(null !=entry.getValue().get("additional") ) {
                            String additionalFiles = String.join(",", (ArrayList) entry.getValue().get("additional"));
                            acell.setCellValue(additionalFiles);
                        }

                    }
                    assetMetadata.clear();
                    versions.clear();
                }

            }


            for(int j = newHeaders.size(); j < newHeaders.size()+(maxCols*2);j+=2 ){
                firstRow.createCell(j).setCellValue("Asset File");
                firstRow.createCell(j+1).setCellValue("Additional Files");

            }

           try {
                FileOutputStream outputStream = new FileOutputStream(fileName);
                batchUploadWorkbook.write(outputStream);
                batchUploadWorkbook.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info(filepath);
    }


    private List<String> getHeaders(Row row) {
        List<String> headers = new ArrayList<>();
        for (Cell current: row) {
            headers.add(current.getStringCellValue());
        }
        return headers;
    }

    private void getCategories() throws IOException {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("categories.xlsx").getFile());

        Workbook workbook;
        try (FileInputStream excelFile = new FileInputStream(new File(file.getAbsolutePath()))) {
            workbook = new XSSFWorkbook(excelFile);
        }

        XSSFSheet dataTypeSheet = (XSSFSheet) workbook.getSheetAt(0);

        for (Row currentRow : dataTypeSheet) {
            if (currentRow.getRowNum() == 0) continue;
                categories.put((int) currentRow.getCell(0).getNumericCellValue(),currentRow.getCell(1).getStringCellValue());
        }

    }
}
