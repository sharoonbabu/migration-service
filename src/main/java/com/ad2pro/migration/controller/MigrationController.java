package com.ad2pro.migration.controller;


import com.ad2pro.migration.service.MigrationService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;






@RestController
public class MigrationController {

    @Autowired
    private MigrationService migrationService;

    @RequestMapping(method = RequestMethod.POST, value = "/migrate")
    public String migrate(@RequestBody JSONObject jsonObject){

        if(null == jsonObject.get("filepath")){
            return "File path not found";
        }

        String filepath = (String) jsonObject.get("filepath");

        migrationService.migration(filepath);

        return "Users migrated";

    }



}
